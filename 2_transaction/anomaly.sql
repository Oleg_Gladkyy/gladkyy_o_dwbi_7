drop database if exists testDB

create database testDB
go

use testDB
go

drop table if exists pharmacy
drop table if exists customers
drop table if exists orders
go

create table pharmacy(
id_pharmacy int not null primary key,
name_pharmacy nvarchar(30) not null,
price decimal(10,2) not null,
amount int not null
)
go

create table customers(
id_customer int not null primary key,
name_customer nvarchar(30) not null,
cash_balance decimal(10,2) not null
)
go

create table orders(
id_order int not null primary key,
id_pharmacy int not null foreign key references pharmacy(id_pharmacy),
id_customer int not null foreign key references customers(id_customer),
amount int not null,
TimeStamp datetime null,
price decimal(10,2) not null
)
go

 

insert into pharmacy(id_pharmacy,name_pharmacy,price,amount) values
(1,'apizatron','222.00',10),
(2,'loratadun','123.00',2),
(3,'akvasprey','300.00',15);
go
insert into customers(id_customer,name_customer,cash_balance) values
(111,'Oleg','2000'),
(119,'Igor','1000'),
(125,'Petro','1500');
go
-----------------------------------------------------------------------
declare @customer int=119
declare @pharmacy int=2
declare @amount int =5

update customers
set cash_balance=cash_balance-@amount*(select price from pharmacy where id_pharmacy=2)
where id_customer=@customer
 
insert into orders values
(1,@pharmacy,119,5,getdate(),@amount*(select price from pharmacy where id_pharmacy=@pharmacy))

update pharmacy
set amount=amount-@amount
where id_pharmacy=@pharmacy

select * from orders
select * from pharmacy
select * from customers
go

------------------------------------
declare @customer int=119
declare @pharmacy int=2
declare @amount int =1
declare @price decimal(10,2)

select @price=price from pharmacy where id_pharmacy=@pharmacy
if @price*@amount<=(select cash_balance from customers where id_customer=@customer)
if @amount<=(select amount from pharmacy where id_pharmacy=@pharmacy)

Begin

update customers
set cash_balance=cash_balance-@amount*(select price from pharmacy where id_pharmacy=2)
where id_customer=@customer
 
insert into orders values
(1,@pharmacy,119,1,getdate(),@amount*(select price from pharmacy where id_pharmacy=@pharmacy))

update pharmacy
set amount=amount-@amount
where id_pharmacy=@pharmacy
 end
 go

select * from pharmacy
select * from customers
select * from orders

-------------------------------------------
declare @customer int=119
declare @pharmacy int=2
declare @amount int =5

begin transaction 

update customers
set cash_balance=cash_balance-@amount*(select price from pharmacy where id_pharmacy=2)
where id_customer=@customer
 
insert into orders values
(1,@pharmacy,119,5,getdate(),@amount*(select price from pharmacy where id_pharmacy=@pharmacy))

update pharmacy
set amount=amount-@amount
where id_pharmacy=@pharmacy

select * from customers where id_customer=@customer
select * from pharmacy where id_pharmacy=@pharmacy

if(select cash_balance from customers where id_customer=@customer)<0 or 
(select amount from pharmacy where id_pharmacy = @pharmacy)<0
begin
print 'lack of money or nutrition'
rollback
end
else 
commit
-----shows changes,but doesn`t make them-----
-----tables stay with the previous data------
declare @customer int=119
declare @pharmacy int=2
declare @amount int =5

select * from customers where id_customer=@customer
select * from pharmacy where id_pharmacy=@pharmacy
---------------------------------------------------------------------
drop table if exists test
go

create table test(
id int not null,
amount int not null
)
go

insert into test values (1,10)
go

while (select amount from test where id=1)>0
begin 
waitfor delay '00:00:01'
update test set amount=amount-1 where id=1
waitfor delay '00:00:01'
end
go

select * from test
go
--------------------------------------------------------------------------
while (select amount from test where id=1)>0
begin 
begin transaction
waitfor delay '00:00:01'
update test set amount=amount-1 where id=1
waitfor delay '00:00:01'
if (select amount from test where id=1)<0
rollback
else
commit
end
go

select * from test
go
-----------------------------------------
BEGIN TRANSACTION
CREATE TABLE tab1(ID int IDENTITY PRIMARY KEY,  Value int)
INSERT INTO tab1 VALUES(1)
SAVE TRANSACTION T1
SELECT * FROM tab1
 
INSERT INTO tab1 VALUES(2)
SELECT * FROM tab1
 
ROLLBACK TRANSACTION T1
SELECT * FROM tab1
 
INSERT INTO tab1 VALUES(3)
SELECT * FROM tab1
 
ROLLBACK
------------------------------------
--deadlock--
BEGIN TRANSACTION 
UPDATE pharmacy
SET Price=300
WHERE id_pharmacy=2
 
WAITFOR DELAY '00:00:10'
 
UPDATE customers
SET cash_balance=500
WHERE id_customer=125
COMMIT
--execute in another query--
BEGIN TRANSACTION 
UPDATE customers
SET cash_balance=500
WHERE id_customer=125

WAITFOR DELAY '00:00:10'

UPDATE pharmacy
SET Price=300
WHERE id_pharmacy=2
Commit
-------------------------------------
--lost update--
BEGIN TRANSACTION 
update pharmacy 
set price =price +25
WAITFOR DELAY '00:00:03'
commit
--execute in another query--
BEGIN TRANSACTION 
update pharmacy 
set price =price +35
WAITFOR DELAY '00:00:03'
commit
-------------------------------------
--Dirty reads---
--first transaction--
select price from  pharmacy where id_pharmacy=1
--second transaction--
update pharmacy
set price='700'
where id_pharmacy=1
--first transaction--
select price from pharmacy where id_pharmacy=1
--second transaction--
rollback
------------------------------
--Non-repeatable reads--
--first transaction--
begin transaction
select * from pharmacy where id_pharmacy=1
--second transaction--
begin transaction
update pharmacy
set price='700'
where id_pharmacy=1
commit
--first transaction--
select * from pharmacy where id_pharmacy=1
commit
------------------------------------------
--Phantom reads--
--first transaction--
select * from customers 
where cash_balance between 700 and 2000
--second transaction--
begin transaction
insert into customers(id_customer,name_customer,cash_balance) values
(113,'Petro','1500');
commit
--first transaction--
select * from customers 
where cash_balance between 700 and 2000
commit
------------------------------------