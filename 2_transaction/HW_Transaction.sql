USE master
go
 
CREATE DATABASE [MyDB] ON
PRIMARY
(NAME = 'Main', FILENAME = 'C:\Epam\Database\Main.mdf'),
 
FILEGROUP [FG]
(NAME = 'FG', FILENAME = 'D:\EPAM\Database\FG.ndf'),
 
FILEGROUP [NDF]
(NAME = 'N1', FILENAME = 'D:\EPAM\Database\N1.ndf'),
(NAME = 'N2', FILENAME = 'D:\EPAM\Database\N2.ndf')
 
LOG ON
(NAME = 'Main_log', FILENAME = 'D:\EPAM\Database\LOG\Main_log.ldf')
go
--------------------------------------------------------------------------------------------------------------
USE [MyDB]
go
 
CREATE TABLE [SysTable] (
ID1 int Primary Key,     Name1 nchar(25)
) ON [PRIMARY]
 
CREATE TABLE [Table1 ] (
ID2 int Primary Key,     Name2 nchar(25)
) ON [FG] 
 
CREATE TABLE [Table2 ] (
ID3 int Primary Key,     Name3 nchar(25)
) ON [FG] 
 
CREATE TABLE [Table3 ] (
ID4 int Primary Key,     Name4 nchar(25)
) ON [NDF]
-------------------------------------------------------------------------------------------------------------
USE  [master]
go

DROP DATABASE IF EXISTS [MyDB]
go

CREATE DATABASE [testDB]
ON   PRIMARY
  ( NAME = testDB,  FILENAME = 'D:\EPAM\Database\testDatabase.mdf',  
    SIZE = 20MB, MAXSIZE = UNLIMITED,  FILEGROWTH = 10MB )
LOG ON 
  ( NAME = testDB_log, FILENAME = 'D:\EPAM\Database\LOG\testDatabase_log.ldf', 
    SIZE = 10MB, MAXSIZE = UNLIMITED,  FILEGROWTH = 20% )
go
-------------------------------------------------------------------------------------------------------------
use testDB
go

create table product
(
id int not null primary key ,
name varchar(50) not null,
quantity int null,
price decimal(6,2)  null,
arrival_time_at_the_warehouse datetime default (getdate()),
update_time_at_the_warehouse datetime null,
check (quantity>=0)

)

create table [order]
(
id_o int not null primary key,
id int not null foreign key references product(id),
number int not null unique,
name varchar(30) not null,
quantity int null,
price decimal(6,2) null,
total as quantity*price,
date_of_order date null default (getdate()),
check (quantity>=0),

)
insert into product (id,name,quantity,price,arrival_time_at_the_warehouse,update_time_at_the_warehouse) 
values
(1,'pen',5,5.50,'',''),
(2,'pencil',2,4.50,'',''),
(3,'book',11,1.75,'',''),
(4,'notebook',12,1.00,'',''),
(5,'stick',50,10.00,'','');

insert into [order] (id_o,id,number,name,quantity,price,date_of_order) 
values
(1,2,56,'oleg',2,4.50,''),
(2,1,58,'igor',5,5.50,''),
(3,4,78,'orest',5,1.00,''),
(4,3,13,'daria',6,1.75,''),
(5,5,55,'tania',33,10.00,'');
-------------------------------------------------------------------------------------------------------------
--check constraint UNIQUE in column number--
UPDATE [order]
SET number = 55
WHERE id_o = 4
--fail--

--check constraint CHECK in column quantity--

UPDATE product
SET quantity = -2
WHERE id = 1
--fail--


--Inspect correct insertion, update and delete data--

insert into product (id,name,quantity,price,arrival_time_at_the_warehouse,update_time_at_the_warehouse) 
values
(6,'just stationery',10,100.50,'','');

--the data is inserted--

UPDATE product
SET price = 00.00
WHERE id = 6
--data refresh--

delete from product
where id = 6

--data deleted--

--script for outputting certain data--
select p.name,o.name
from product p
join [order] o ON p.id = o.id
where total >200.00
order by total desc



