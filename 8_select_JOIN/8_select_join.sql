use labor_sql
go
--------------------------------
--------------1-----------------
select p.maker,p.type,pc.speed,pc.hd
from product p
inner join pc  on p.model=pc.model
where pc.hd<=8
--------------------------------
--------------2-----------------
select p.maker
from product p
inner join pc on p.model=pc.model
where pc.speed>=600
--------------------------------
--------------3-----------------
select p.maker
from product p
inner join laptop l on p.model=l.model
where l.speed<=500
--------------------------------
--------------4-----------------
select distinct p1.model, p2.model, p1.speed, p1.ram
from pc p1, pc p2
where p1.speed = p2.speed and p1.ram = p2.ram and p1.model > p2.model
--------------------------------
--------------5-----------------
select country
from classes
group by country
having count(distinct type) = 2
--------------------------------
--------------6-----------------
select p.maker,pc.model
from product p
inner join pc on p.model=pc.model
where pc.price<600
--------------------------------
--------------7-----------------
select p.maker,pr.model
from product p
inner join printer pr on p.model=pr.model
where pr.price>300
--------------------------------
--------------8-----------------
select p.maker,pc.model,pc.price
from product p
inner join pc  on p.model=pc.model
--------------------------------
--------------9-----------------
select p.maker,pc.model,pc.price
from product p
left join pc  on p.model=pc.model
where p.[type] = 'pc'
go
--------------------------------
-------------10-----------------
select p.maker,p.type,l.model,l.speed
from product p
inner join laptop l on p.model=l.model
where l.speed >600
--------------------------------
-------------11-----------------
select s.name,c.displacement
from ships s
inner join classes c on s.class=c.class
--------------------------------
-------------12-----------------
select o.ship,b.date
from outcomes o
inner join battles b on o.battle=b.name
where o.result in('OK','damaged')
--------------------------------
-------------13-----------------
select s.name ,c.country
from ships s 
inner join classes c on s.class=c.class
--------------------------------
-------------14-----------------
select  t.plane ,c.name
from trip t
inner join company c on t.id_comp=c.id_comp
where t.plane='Boeing'
--------------------------------
-------------15-----------------
select p.name, cast(pt.date as date) [date]
from passenger p
inner join pass_in_trip pt on p.id_psg=pt.id_psg
--------------------------------
-------------16-----------------
select pc.model,pc.speed,pc.hd
from product p
inner join pc on p.model=pc.model
where pc.hd in('10','20') and p.maker='A'
--------------------------------
-------------17-----------------
select *
from (select  maker,type from product)as info 
pivot (count([type])
for [type] in ([pc],[laptop],[printer]))as PVT_table
--------------------------------
-------------18-----------------
select *
from (select price as avg_,screen from laptop) as info
pivot(avg(avg_)
for screen in([11],[12],[14],[15]))as PVT_table
--------------------------------
-------------19-----------------
select p.maker,l.* from product p 
cross apply(select * from laptop l where p.model=l.model)l
--------------------------------
-------------20-----------------
select*
from laptop l1
 CROSS APPLY
 (select  max(price) max_price from Laptop l2
inner join  product p1 ON l2.model=p1.model 
where maker = (select  maker from  Product p2 where p2.model= l1.model)) x
--------------------------------
-------------21-----------------
select * from laptop l1 
cross apply(select top 1 * from laptop l2
where l1.model<l2.model or (l1.model =l2.model and l1.code<l2.code)
order by model,code)x
order by l1.model
--------------------------------
-------------22-----------------
select * from laptop l1 
outer apply(select top 1 * from laptop l2
where l1.model<l2.model or (l1.model =l2.model and l1.code<l2.code)
order by model,code)x
order by l1.model
--------------------------------
-------------23-----------------
select x.* from (select distinct type from product)p1
cross apply (select top 3* from product p2 where p1.type=p2.type
order by p2.model)x
--------------------------------
-------------24-----------------
SELECT code, name, value FROM Laptop
CROSS APPLY
(VALUES('speed', speed)
,('ram', ram)
,('hd', hd)
,('screen', screen)
) spec(name, value)
WHERE code < 4 
ORDER BY code, name, value


