
--created the trigger that blocks the data update in the column 'update_time_at_the_warehouse'--
create trigger UpdateDates ON product
after UPDATE
as
if UPDATE(update_time_at_the_warehouse)
begin
	  print 'error,you can`t update this column('
      rollback transaction
       
end

--checked trigger--
update product
set update_time_at_the_warehouse = getdate()
where update_time_at_the_warehouse = '1900-01-01 00:00:00.000'
--result:error--

--drop trigger--
drop trigger if exists UpdateDate

--created the trigger that modifies 'update_time_at_the_warehouse'--
create trigger update_date on product
after insert
as
declare @update_time_at_the_warehouse datetime
select @update_time_at_the_warehouse= update_time_at_the_warehouse from inserted
if @update_time_at_the_warehouse<= getdate()
begin
print 'error insert'
rollback transaction
end

--check--
insert into product (id,name,quantity,price,arrival_time_at_the_warehouse,update_time_at_the_warehouse) 
values 
(6,'red book',100,3.00,'1900-01-01 00:00:00.000','2018-07-18 11:11:11.111');
--result:error--

--create view--
create view Sum_Price 
as
select sum(quantity) as Sum_Price
from product

go

--check--
select * from Sum_Price

--create view with check option--
alter view New_view
as
SELECT *
  FROM product
  WHERE name= 'book'
  WITH CHECK OPTION
  go

  --check--
insert into New_view (id,name,quantity,price,arrival_time_at_the_warehouse)
values
(10,'book',100,3.00,'1900-01-01 00:00:00.000');


--result:error--

--create some tables-

create table person(
gradebook int not null primary key,
name varchar(20) not null,
surname varchar(20) not null,
date_of_birth date null,
address varchar(50) not null,
phone char(10) not null UNIQUE,
chair char(2) not null,
specialty varchar(20) not null,
form_of_study char(1) not null,
date_of_admission date not null default getdate(),
date_of_fire date null
)

create table test(
id_t int not null primary key,
subject varchar(20) not null,
[date] date null,
[time] time null, 
audience char(3) null,
gradebook int not null foreign key references person(gradebook) 
)

create table result(
gradebook int not null foreign key references person(gradebook),
id_e int not null foreign key references test(id_t),
mark decimal(6,2) null
)

insert into person(gradebook,name,surname,date_of_birth,address,phone,chair,specialty,form_of_study)
values
(1,'a','b','1999-07-08','vrub 15','0734945682','IT','programmer','d'),
(2,'d','f','1998-06-18','aaa 666','0508745682','IT','programmer','n'),
(3,'h','j','1999-07-28','pasichna 111','0504945682','ET','economic','n'),
(4,'f','t','1997-12-12','vod 5','0676785682','ET','economic','d'),
(5,'j','j','2012-12-12','kerch 1','0734940002','IT','programmer','d');

insert into test (id_t,subject,[date],[time],audience,gradebook)
values 
(1,'c++','2018-07-18','12:00:00','412',1),
(2,'C#','2018-06-18','13:30:00','412',2),
(3,'BI','2018-07-20','12:20:00','410',3),
(4,'DB','2018-07-17','11:00:00','312',4),
(5,'java','2018-06-18','09:00:00','312',5);

insert into result(gradebook,id_e,mark) values
(1,1,'12'),
(2,2,'2'),
(3,3,'3'),
(4,4,'4'),
(5,5,'10');

------------------------------------------------------------------------------------------------------------------------------------


--create triggers (update, delete, insert)--

--create trigger for update--
create trigger UpdateDates ON test
after UPDATE
as
if UPDATE([date])
begin
	  print 'error,you can`t update this column('
      rollback transaction
     end  
	
	 
	 --check trigger for update--
	 update test
	 set [date] = getdate()
	 where [date] = '2018-07-18'
	  --result: error--


	  --create trigger for delete--

	 create trigger delInformation on test
	 after delete 
	 as
	 if EXISTS(select * from deleted where id_t > 0)
	 begin 
		print 'error, you can`t delete from this table'
		rollback transaction 
		end

		--check trigger for delete--
	 delete from test
	 where id_t = 5
		--result: error--


		--create trigger for insert--
		
	create trigger insert_gradebook on test
	after insert
	as
	declare @insert_gradebook int
	select @insert_gradebook = gradebook from inserted
	if @insert_gradebook > 0
	begin
	print 'error insert'
	rollback transaction
	end

		--check trigger for insert--

		insert into test(id_t, subject, [date],[time], audience, gradebook)
		values(6,'f', '2018-07-11', '12:00:00', 120, 6)
		--result:error--



		--select for check constraints--
		select t.subject, t.[date], p.gradebook, p.phone
		from test t 
		inner join person p on p.gradebook = t.gradebook
		where p.date_of_birth  > '1999-07-08'
		--successfull--

		--create synonyms--
		create synonym dbo.per for testDB.dbo.person
		go
		
		create synonym dbo.ts for testDB.dbo.test
		go
		
		create synonym dbo.res for testDB.dbo.result
		go

		--#1--
		select name, surname, phone
		FROM dbo.per
		where chair = 'IT'

		--#2--
		select id_t, subject, [date]
		from dbo.ts
		where audience = '312'

		--#3--
		select gradebook, id_e 
		from dbo.res
		where mark > 2.00

		--#4--
		select p.name, t.[date], r.mark
		from dbo.per p
		inner join dbo.ts t on t.gradebook = p.gradebook 
		inner join dbo.res r on r.gradebook = p.gradebook 
		where r.mark > 4.00
		order by p.name