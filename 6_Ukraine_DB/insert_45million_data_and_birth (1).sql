use [people_ua_db]
go
---------------- VIEWS  --------------------
create or alter view [m_name]
as 
select * from [name_list_identity]
where [sex] = 'm'
go
create or alter view [w_name]
as
select * from [name_list_identity]
where [sex] = 'w'


go
declare @m_names table (id int identity,[name] nvarchar(20)) 
insert into @m_names select [name] from [m_name]
declare @w_names table (id int identity,[name] nvarchar(20)) 
insert into @w_names select [name] from [w_name]
declare @count int
declare @name_count int = (select count(*) from [name_list_identity])
declare @m_name_count int = (select count(*) from @m_names)
declare @w_name_count int = (select count(*) from @w_names)
declare @random_name table( id int, [name] nvarchar(20), [sex] nchar(1))
declare @birth_day varchar(10)
declare @region_id int
declare @surname nvarchar(20)
declare @amount int
declare @sex nchar(1)
declare @name nvarchar(20)
declare @random_name_id int
declare @year int
declare @month int
declare @day int 
declare @days_amount int = 31
declare [insert_data] cursor for 
                         select [surname],[sex],[amount] from [surname_list_identity]
open [insert_data]
fetch next from [insert_data] into @surname, @sex , @amount
     while @@FETCH_STATUS = 0
	 BEGIN
	      while @amount > 0
		  BEGIN
		  set @year  = (1925+ (SELECT TOP 1 ABS(CONVERT(INT, (CONVERT(BINARY(1), (NEWID()))))) %75
               FROM sysobjects A
               CROSS JOIN sysobjects B))
          set @month = (1+ (SELECT TOP 1 ABS(CONVERT(INT, (CONVERT(BINARY(1), (NEWID()))))) % 12
               FROM sysobjects A
               CROSS JOIN sysobjects B))
			   if @month = 2
                   begin
				   set @days_amount = 28
				   end
          set @day =   (1+ (SELECT TOP 1 ABS(CONVERT(INT, (CONVERT(BINARY(1), (NEWID()))))) % @days_amount
               FROM sysobjects A
               CROSS JOIN sysobjects B))
	      set @birth_day = CONCAT(@year,'-',@month ,'-',@day) 
			   PRINT @birth_day
          set @region_id = (1+ (SELECT TOP 1 ABS(CONVERT(INT, (CONVERT(BINARY(1), (NEWID()))))) % (select count(*) from region)
               FROM sysobjects A
               CROSS JOIN sysobjects B))
	      IF  @sex <> NULL
	          BEGIN
	          if @sex = 'm'
		         begin
		         set @name = (select [name] from @m_names where id = (1+(SELECT TOP 1 ABS(CONVERT(INT, (CONVERT(BINARY(1), (NEWID())))))% @m_name_count
                 FROM sysobjects A
                 CROSS JOIN sysobjects B))) 
		      end
		      if @sex = 'w'
		      begin
		         set @name = (select [name] from @w_names where id = (1+(SELECT TOP 1 ABS(CONVERT(INT, (CONVERT(BINARY(1), (NEWID())))))% @w_name_count
                 FROM sysobjects A
                 CROSS JOIN sysobjects B)))
		      end
	          END
	      ELSE
	      begin
	      set @random_name_id  = (1+(SELECT TOP 1 ABS(CONVERT(INT, (CONVERT(BINARY(1), (NEWID())))))% @name_count
               FROM sysobjects A
               CROSS JOIN sysobjects B))
          set @name = (select [name] from [name_list_identity] where id = @random_name_id)
	      set @sex  = (select [sex]  from [name_list_identity] where id = @random_name_id)
	      end
		  insert into people([surname],[name],[sex],[GUID],[region_id],[birth_day],[date_creation]) values
		                   (@surname,@name,@sex,NEWID(),@region_id,@birth_day,GETDATE())
	          set  @count  += 1	
			       if @count = 10000
				   begin
				   PRINT ' ------------ 10000 person was added ------------'
				   set @count = 0
				   end		
		      set  @amount -= 1
		  END
	 fetch next from [insert_data] into @surname, @sex , @amount 
	 END
close [insert_data]
deallocate [insert_data]
PRINT ' ------------ ALL PERSONS WAS ADDED ------------ ' 
go

select * from people
