use master
go
create database OG_Library
go

use OG_library
go

create schema OG_library
go


GO
CREATE SEQUENCE OG_library.seq_ym_library
    START WITH 1  
    INCREMENT BY 1 ;  
GO


create table authors(
Author_Id int primary key not null,
Name varchar(50) unique not null,
URL	varchar(255) not null default('www.author_name.com'),
inserted	date not null default (getdate()),
inserted_by	varchar(50) not null default(system_user),
updated	date,
updated_by	varchar(50),
)
go

create table Publishers(
Publisher_Id int primary key not null,
Name varchar(50) unique not null,
url varchar(50) default 'wwwpublisher_name.com' not null,
seq_no int  default 0 check (seq_no>=0) not null,
inserted date not null default getdate(),
inserted_by	varchar(50) not null default system_user,
updated	date,
updated_by	varchar(50)
)
go

create table books(
ISBN varchar(50) primary key not null,
Publisher_Id int foreign key references publishers(publisher_id) not null,
URL	varchar(255) unique not null,
Price	decimal(15,3) default 0 check (price>=0) not null,
inserted	date not null default getdate(),
inserted_by	varchar(50) default system_user,
updated	date,
updated_by	varchar(50)
)
go


create table BooksAuthors(
BooksAuthors_id	int primary key default 1 check(booksauthors_id >=0) not null,
ISBN varchar(50) unique,
author_id int unique not null,
seq_no int  default 0 check (seq_no>=0) not null,
inserted date not null default getdate(),
inserted_by	varchar(50) not null default system_user,
updated	date,
updated_by	varchar(50),
constraint fk_isbn foreign key(isbn) references books(isbn) on update cascade on delete no action,
constraint fk_author_id foreign key(author_id) references authors(author_id) on update cascade on delete no action
)
go



create table Authors_log(
operation_id  int identity primary key not null,
Author_Id_new int,
Name_new varchar(50),
URL_new varchar(50),
Author_Id_old int,
Name_old varchar(50),
URL_old varchar(50),
operation_type varchar(1) check (operation_type in('I','D','U')) not null,
operation_datetime datetime default getdate() not null
)
go
 

create trigger tg_authors
on authors
after insert, update
as 
begin
declare @author_id int
select @author_id=author_id from inserted
if @author_id is null
select @author_id=author_id from deleted
update authors set updated=getdate(), updated_by=SYSTEM_USER
where author_id=@author_id
end
go



create trigger tg_authors_log
on authors_log
after delete
as 
begin
print 'Deleting to this table is limited' 
rollback
end
go



CREATE VIEW v_authors
AS
SELECT        dbo.authors.*
FROM            dbo.authors

GO


USE OG_Library
GO
CREATE synonym s_authors
FOR            dbo.authors

GO