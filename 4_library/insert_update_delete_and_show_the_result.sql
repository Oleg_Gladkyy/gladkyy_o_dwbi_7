--insert data--
select * from authors

insert into authors (Author_Id,Name,URL) values
(1,'shevchenko','sheva.com'),
(2,'bas',default),
(3,'gladkyy',default),
(4,'ukrainka','ukr.com'),
(5,'skovoroda','skovoroda.com'),
(6,'kotliarevckii','kotl.com'),
(7,'markevych',default),
(8,'franko','fr.com'),
(9,'poet',default),
(10,'NePoet','yanepoet.com');

select * from authors
-------------------------
select * from Publishers

insert into Publishers(Publisher_Id,Name,url)values
(1,'Svitanok',default),
(2,'1Veresnya','1v.com'),
(3,'goodmorning',default),
(4,'thisisxarasho',default),
(5,'publisher1','pub1.com'),
(6,'publisher2','pub2.com'),
(7,'publisher3',default),
(8,'bookslike','liker.com'),
(9,'bla','bla.com'),
(10,'blabla',default);

select * from Publishers
---------------------------------
select * from books

insert into books(ISBN,Publisher_Id,URL,Price)values
('111-11111-22222-1',1,'book1.com',10.00),
('222-22222-22222-2',2,'book2.com',20.00),
('333-33333-33333-3',3,'book3.com',30.00),
('444-44444-44444-4',4,'book4.com',40.00),
('555-55555-55555-5',5,'book5.com',50.00),
('666-66666-66666-6',6,'book6.com',60.00),
('777-77777-77777-7',7,'book7.com',70.00),
('888-88888-88888-8',8,'book8.com',80.00),
('999-99999-99999-9',9,'book9.com',90.00),
('000-00000-00000-0',10,'book0.com',99.00);

select * from books
-----------------------------------------------
select * from BooksAuthors

insert into BooksAuthors(BooksAuthors_id,ISBN,author_Id,seq_No) values
(1,'000-00000-00000-0',1,2),
(2,'999-99999-99999-9',2,1),
(3,'888-88888-88888-8',3,3),
(4,'777-77777-77777-7',4,4),
(5,'666-66666-66666-6',5,5),
(6,'555-55555-55555-5',6,7),
(7,'444-44444-44444-4',7,6),
(8,'333-33333-33333-3',8,10),
(9,'222-22222-22222-2',9,9),
(10,'111-11111-22222-1',10,8);

select * from BooksAuthors
---------------------------------------------------------------------------------------
--update data--
select * from authors

update authors
set URL='basok.com'
where Name='bas'

select * from authors
-----------------------
select * from Publishers

update Publishers
set Name='bestpublisher'
where Publisher_Id=5

select * from Publishers
--------------------------
select * from books

update books
set price ='100.00'
where URL='book0.com'

select * from books
---------------------------
--delete data--

SELECT * FROM Authors;

DELETE FROM Authors
WHERE Author_Id>=9;

SELECT * FROM Authors;
--------------------------
select * from Publishers
delete from Publishers
where Name='Svitanok'
select * from Publishers
-----------------------------
select*from books
delete from books
where Publisher_Id >8
select* from books
---------------------------

