USE IB_library;
GO


CREATE trigger book_amount_definition ON Books
AFTER INSERT, UPDATE, DELETE
AS
BEGIN

	UPDATE Publishers
	SET Publishers.book_amount=COALESCE((SELECT COUNT(*) FROM Books
	                            WHERE Books.Publisher_Id=Publishers.Publisher_Id),0)
	WHERE Publishers.Publisher_Id IN (SELECT Publisher_Id FROM inserted UNION ALL SELECT Publisher_Id FROM deleted);

	UPDATE Publishers
	SET Publishers.issue_amount=COALESCE((SELECT COUNT(*) FROM Books
	                            WHERE Books.Publisher_Id=Publishers.Publisher_Id),0)
	WHERE Publishers.Publisher_Id IN (SELECT Publisher_Id FROM inserted UNION ALL SELECT Publisher_Id FROM deleted);

	UPDATE Publishers
	SET Publishers.total_edition=COALESCE((SELECT SUM(edition) FROM Books
	                            WHERE Books.Publisher_Id=Publishers.Publisher_Id),0)
	WHERE Publishers.Publisher_Id IN (SELECT Publisher_Id FROM inserted UNION ALL SELECT Publisher_Id FROM deleted);
END

update books
set edition='2.00'
where Publisher_Id=10

select * from books
select * from Publishers
