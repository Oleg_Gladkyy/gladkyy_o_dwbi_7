alter table authors
add birthday date null,
book_amount numeric(6,2) not null default '0',
issue_amount numeric(6,2) not null default '0',
total_edition numeric(6,2) not null default '0',
check (book_amount >=0),
check (issue_amount >=0),
check (total_edition >=0)

alter table books
add title nvarchar(50) not null default 'title',
edition numeric(6,2) not null default '1' ,
publishet date null,
issue int  null,
check(edition>=1)

alter table Publishers
add created date null default '19000101',
country varchar(20) null default 'USA',
city varchar(20) null default 'NY',
book_amount numeric(6,2) not null default '0',
issue_amount numeric(6,2) not null default '0',
total_edition numeric(6,2) not null default '0',
check (book_amount >=0),
check (issue_amount >=0),
check (total_edition >=0)

alter table Authors_log
add book_amount_old numeric(6,2)  null ,
issue_amount_old numeric(6,2)  null ,
total_edition_old numeric(6,2)  null ,
book_amount_new numeric(6,2)  null ,
issue_amount_new numeric(6,2)  null ,
total_edition_new numeric(6,2)  null 



