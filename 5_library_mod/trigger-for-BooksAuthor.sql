USE IB_library
GO


CREATE trigger book_amount_definition_for_authors ON Books
AFTER INSERT, UPDATE, DELETE
AS
BEGIN

	UPDATE Authors
	SET Authors.book_amount=COALESCE((SELECT COUNT(*) FROM Books as b
	                                  inner join BooksAuthors as ba
									    on b.ISBN=ba.ISBN and ba.Author_id=Authors.Author_id),0)

	WHERE Authors.Author_id IN (SELECT * FROM(SELECT ba.Author_id FROM 
								inserted as i inner join BooksAuthors as ba
								on i.ISBN=ba.ISBN) as INS 
								
								UNION ALL
								
								SELECT * FROM(SELECT ba.Author_id FROM 
								deleted as d inner join BooksAuthors as ba
								on d.ISBN=ba.ISBN) as DEL);

    UPDATE Authors
	SET Authors.issue_amount=COALESCE((SELECT COUNT(*) FROM Books as b
	                                  inner join BooksAuthors as ba
									    on b.ISBN=ba.ISBN and ba.Author_id=Authors.Author_id),0)

	WHERE Authors.Author_id IN (SELECT * FROM(SELECT ba.Author_id FROM 
								inserted as i inner join BooksAuthors as ba
								on i.ISBN=ba.ISBN) as INS 
								
								UNION ALL
								
								SELECT * FROM(SELECT ba.Author_id FROM 
								deleted as d inner join BooksAuthors as ba
								on d.ISBN=ba.ISBN) as DEL);

	UPDATE Authors
	SET Authors.total_edition=COALESCE((SELECT SUM(b.edition) FROM Books as b
	                                  inner join BooksAuthors as ba
									    on b.ISBN=ba.ISBN and ba.Author_id=Authors.Author_id),0)

	WHERE Authors.Author_id IN (SELECT * FROM(SELECT ba.Author_id FROM 
								inserted as i inner join BooksAuthors as ba
								on i.ISBN=ba.ISBN) as INS 
								
								UNION ALL
								
								SELECT * FROM(SELECT ba.Author_id FROM 
								deleted as d inner join BooksAuthors as ba
								on d.ISBN=ba.ISBN) as DEL);

    

	
END


update books
set edition='10.00'
where Publisher_Id=2
select * from books
select * from Publishers



